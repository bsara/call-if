# 1.0.4

- **[FIXED]** syntax issue in `index.js`.



# 1.0.1, 1.0.2, 1.0.3

- Added documentation to README and added changelog.



# 1.0.0

- Initial release.
